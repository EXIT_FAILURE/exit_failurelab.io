# generates an html index file with all files in ROOT, recursively

import re
from pathlib import Path
import codecs
import html
# import urllib

MISSION_NAME_REGEX = re.compile(r'mission.*?{.*?name:t="(.*?)(?<!\\)"', flags=re.DOTALL)
ROOT = "public"
TEMPLATE = (
    r'<!doctype html>'
    r'<html lang="en">'
    r'<head>'
    r'<meta charset="utf-8">'
    r'<title>War Thunder missions</title>'
    r'<style type="text/css">'
    r'body {{ font-family: sans-serif; font-size: 1em; }}'
    r'#files {{ list-style: none; }}'
    r'.file-url {{ font-family: monospace; }}'
    r'</style>'
    r'</head>'
    r'<body>'
    r'<header>'
    r'<h1>War Thunder missions hosted here:</h1>'
    r'<em>Hint: right click and copy the link, then paste it in the game.</em>'
    r'</header>'
    r'<ul id="files">'
    r'{file_list}'
    r'</ul>'
    r'</body>'
    r'</html>'
)

def main():
    root_path = Path(ROOT)
    index_file = root_path / "index.html"
    indexed_dir = root_path / "wt"

    # if index already present, stop gen
    if index_file.exists():
        return

    def make_li(path):
        title = "N/D mission title"

        # get mission title from file with regex
        with codecs.open(path) as mission_file:
            find_title = MISSION_NAME_REGEX.search(mission_file.read())

            if find_title:
                title = find_title.group(1)

        li_template = r'<li><a class="file-url" href="{url}">[{file_name}]</a> Title: {mission_title}</li>'
        return li_template.format(
            url=html.escape(str(path.relative_to(root_path))),
            file_name=html.escape(str(path.relative_to(indexed_dir))),
            mission_title=html.escape(title, quote=True)
        )

    all_except_dotfile = [item for item in indexed_dir.rglob("*") if (item.is_file() and not str(item.name).startswith("."))]
    index_out = TEMPLATE.format(file_list="".join([make_li(path) for path in all_except_dotfile]))
    with codecs.open(index_file, mode="w", encoding="utf8") as out_file:
        out_file.write(index_out)

if __name__ == "__main__":
    main()
