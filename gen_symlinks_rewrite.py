import os
from pathlib import Path
import shutil

BLK_ROOT = "public/blk"
SYMLINK_ROOT = "public/wt"

def main():
    blk_path = Path(BLK_ROOT)
    symlink_path = Path(SYMLINK_ROOT)

    for path in blk_path.rglob("*.blk"):
        dst_parent_relative = path.parent.relative_to(blk_path)

        (symlink_path / dst_parent_relative).mkdir(parents=True, exist_ok=True)

        shutil.copy2(str(path), str(symlink_path / dst_parent_relative / str(path.stem).replace(" ", "_")))

if __name__ == "__main__":
    main()